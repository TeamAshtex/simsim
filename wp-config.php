<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'simsim');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', '12345');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'J!|L`]0}1wp$D@F}+x.6Duwv*+?X({Z>>+>J]~+DPUg0Ztk(PIPO(W]-U!u?gO-[');
define('SECURE_AUTH_KEY',  'dwm5Y%<Xv%>C.t>5S/^Zu:eG#}pwlfB5[oMb-_v9/kN6_7YN*o%Lp*m^u*l~KOpB');
define('LOGGED_IN_KEY',    'bY%P**LNi}7b8yeuaq x/vqt![$]L2(xH5*++:n0YlW6l}I}k@ES6v2nJ}L0[l=n');
define('NONCE_KEY',        'O)E,-<<$zuB!$gg09W/4TQ4j>(yyTGs{EM|2j`r^4][f~nt!TT7Z.:Wr+t54;@,|');
define('AUTH_SALT',        'u$xkR5:ZIL%pDH^*k[`C>[dOb<aAJe!W3Kw%j+x4E7#>4jq/Ii+N)O{N_8rgp5<X');
define('SECURE_AUTH_SALT', '1z%|j_nb[<,d)3-}ZJ%e~MRWCSgY-Q|9fl&HLY(LMbXnyLuJoN/IHL+W} ieZ^o-');
define('LOGGED_IN_SALT',   '/n1bEBtXFuh8Qn#9KIl:d~2X])<et_rqF^yl15^fvIX&2xo;-ksMh=F.$/R`Qer8');
define('NONCE_SALT',       'k(b)y%RVz&%7R.a/ShQmMN>V4mXr6wJLWGX<xDW!iE:MF{,/sf(}J;j?LeJx,xrh');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
